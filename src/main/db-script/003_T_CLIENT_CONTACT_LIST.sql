USE [BrokerDB]
GO

/****** Object:  Table [dbo].[T_CLIENT_CONTACT_LIST]    Script Date: 17-10-2018 10:43:44 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[T_CLIENT_CONTACT_LIST](
	[ID] [int] IDENTITY(101,1) NOT NULL,
	[CLIENT_ID] [int] NOT NULL,
	[CLIENT_CONTACT] [nvarchar](100) NULL,
	[PHONE] [nvarchar](50) NULL,
	[FAX] [nvarchar](50) NULL,
	[EMAIL] [nvarchar](50) NULL,
	[LOCATION] [nvarchar](200) NOT NULL,
	[CONTACT_POINT] [nvarchar](150) NULL,
	[NOTES] [nvarchar](500) NULL,
	[STATUS] [nvarchar](50) NULL,
	[CREATED_BY] [nvarchar](50) NULL,
	[CREATED_ON] [datetime] NULL,
	[MODIFIED_BY] [nvarchar](50) NULL,
	[MODIFIED_ON] [datetime] NULL,
	[ATTRIBUTE1] [nvarchar](100) NULL,
	[ATTRIBUTE2] [nvarchar](100) NULL,
	[ATTRIBUTE3] [nvarchar](100) NULL,
	[TAB_ID] [int] NULL,
	[TAB_NAME] [nvarchar](100) NULL,
	[VERSION] [int] NOT NULL,
	[CATEGORY] [int] NOT NULL,
	[COLORED] [int] NOT NULL,
 CONSTRAINT [PK_T_CLIENT_CONTACT_LIST] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

