package com.isit.prf.batch;

import javax.persistence.EntityManagerFactory;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.database.JpaItemWriter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.isit.prf.entity.ClientMaster;

@Configuration
public class ExcelBatch {


	
	
	@Bean(name = "excelItemReader")
	public ExcelItemReader reader() {
		return new ExcelItemReader();
	}
	


	@Bean(name="excelItemWriter")
	public JpaItemWriter<ClientMaster> namedInsuredWriter(EntityManagerFactory emf) {
		JpaItemWriter<ClientMaster> writer = new JpaItemWriter<>();
		writer.setEntityManagerFactory(emf);
		return writer;
	}
	


	@Bean
	public ItemProcessor<XSSFWorkbook, ClientMaster> excelProcessor() {
		return new ExcelItemProcessor();
	}
	
}
