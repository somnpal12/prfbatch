package com.isit.prf.batch;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ItemReader;
import org.springframework.beans.factory.annotation.Autowired;

import com.isit.prf.batch.util.AuditorAwareImpl;
import com.isit.prf.data.ClientMasterRepository;
import com.isit.prf.entity.ClientMaster;
import com.isit.prf.util.ConstantUtil;

import lombok.extern.slf4j.Slf4j;



@Slf4j
public class ExcelItemReader implements ItemReader<XSSFWorkbook> {

	@Autowired
	AuditorAwareImpl auditorAware;
		
	@Autowired
	ClientMasterRepository clientMasterRepository;
	
	
	List<ClientMaster> clientMasterList = null;
	ClientMaster clientMaster = null;
	
	StepExecution stepExecution;
	
	int counter = 0;
	
	@BeforeStep
	private void initialize(StepExecution pStepExecution) {
		this.stepExecution = pStepExecution;
		counter = 0;
		
		clientMasterList = clientMasterRepository.findByStatus(ConstantUtil.SEARCHBY.getValue());
		log.debug("records fetch :: {}" , clientMasterList.size());
	}

	@Override
	public XSSFWorkbook read() throws Exception {
		XSSFWorkbook workbook = null ;
		
		if(counter < clientMasterList.size()) {
			clientMaster = clientMasterList.get(counter++);
			auditorAware.setClientMaster(clientMaster);
			stepExecution.getExecutionContext().put("CLIENT_MASTER", clientMaster.getClientId());
			log.debug("processing record for {}",clientMaster.getClientId());
			log.debug("processing file {}",clientMaster.getFilepath());
			try (InputStream iostream = new FileInputStream(new File(clientMaster.getFilepath()))) {
				workbook = new XSSFWorkbook(iostream);
			}catch (IOException e) {
				log.error("INIT ERR :: ");
				throw e;
			}
			
		}
		
		return workbook;
	}

}
