package com.isit.prf.batch.util;

import java.util.Optional;

import org.springframework.data.domain.AuditorAware;

import com.isit.prf.entity.ClientMaster;

public class AuditorAwareImpl implements AuditorAware<String>{

	ClientMaster clientMaster = null;
	
	@Override
	public Optional<String> getCurrentAuditor() {
		
		return  Optional.ofNullable(this.clientMaster.getAddedBy());
	}
	
	public void setClientMaster(ClientMaster clientMaster) {
		this.clientMaster = clientMaster;
	}

}
