package com.isit.prf.batch.util;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Optional;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.beans.factory.annotation.Autowired;

import com.isit.prf.data.ClientMasterRepository;
import com.isit.prf.entity.ClientMaster;
import com.isit.prf.util.ConstantUtil;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ExcelStepListener implements StepExecutionListener{

	@Autowired
	ClientMasterRepository clientMasterRepository;
	
	Optional<ClientMaster> o ;
	ClientMaster clientMaster;
	Long id ;
	
	@Override
	public void beforeStep(StepExecution stepExecution) {
		log.debug("ExcelStepListener Start");
	}

	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
		
		String fileOrgPath = null; 
		String fileErrPath = null; 
		String filename = null;
		
		
		log.debug("inside ExcelStepListener : step execution status :: {}",stepExecution.getExitStatus().getExitCode());
		
		if(ExitStatus.FAILED.getExitCode().equals(stepExecution.getExitStatus().getExitCode())) {
			id = (Long)stepExecution.getExecutionContext().get("CLIENT_MASTER");
			log.info("processing data for client master ({}) for Error ",id);
			o  = clientMasterRepository.findById(id);
			if(o.isPresent()) {
				clientMaster = o.get();
				fileOrgPath = clientMaster.getFilepath();
				
				filename = StringUtils.substringAfterLast(fileOrgPath, "\\");
				
				fileErrPath = StringUtils.substringBeforeLast(fileOrgPath, "\\") + "\\error\\" + filename ;
				
						
				log.debug("error path {}" , fileErrPath);
				
				 File source =  FileUtils.getFile(fileOrgPath);
				 File target =  FileUtils.getFile(fileErrPath);
			     try {
					FileUtils.moveFile(source, target);
				} catch (IOException e) {
					log.error("Unable to move file {}" , fileOrgPath , e);
				}
				
				clientMaster.getPrfUserActivity().setStatus(ConstantUtil.ERRSTATUS.getValue());
				clientMaster.getPrfUserActivity().setDisabledBy(ConstantUtil.CREATEDBY.getValue());
				clientMaster.getPrfUserActivity().setDisabledDate(new Date());
				clientMasterRepository.save(clientMaster);
				
			}
		}
		
		
		
		return stepExecution.getExitStatus();
	}

}
