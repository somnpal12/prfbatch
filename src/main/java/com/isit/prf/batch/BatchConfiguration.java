package com.isit.prf.batch;

import javax.persistence.EntityManagerFactory;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import com.isit.prf.batch.util.AuditorAwareImpl;
import com.isit.prf.batch.util.ExcelStepListener;
import com.isit.prf.entity.ClientMaster;

import lombok.extern.slf4j.Slf4j;

@Configuration
@EnableBatchProcessing
@Slf4j
@EnableScheduling
@EnableJpaAuditing(auditorAwareRef = "auditorAware")
public class BatchConfiguration {

	@Autowired
	public JobBuilderFactory jobBuilderFactory;

	@Autowired
	public StepBuilderFactory stepBuilderFactory;

	@Autowired
	EntityManagerFactory entityManagerFactory;
	
	@Autowired
	private ExcelBatch excelBatch;

	@Autowired
	JobLauncher jobLauncher;

	@Bean(name="auditorAware")
	public AuditorAware<String> auditorAware() {
		return new AuditorAwareImpl();
	}

	@Scheduled(fixedRate = 180000)
	public void scheduleBatch() throws JobExecutionAlreadyRunningException, JobRestartException,
			JobInstanceAlreadyCompleteException, JobParametersInvalidException {
		JobExecution exec = jobLauncher.run(excelLoaderJobBuilder(),
				new JobParametersBuilder().addLong("uniqueId", System.currentTimeMillis()).toJobParameters());

		log.info("Batch Finish at {}", exec.getEndTime().toString());
	}

	public Job excelLoaderJobBuilder() {
		return jobBuilderFactory.get("excelLoadJob").incrementer(new RunIdIncrementer()).flow(excelLoaderStepBuilder())
				.end().build();
	}

	public Step excelLoaderStepBuilder() {
		return stepBuilderFactory.get("excelLoaderStep").<XSSFWorkbook, ClientMaster>chunk(21)
				.reader(excelBatch.reader()).processor(excelBatch.excelProcessor())
				.writer(excelBatch.namedInsuredWriter(entityManagerFactory)).listener(getStepExecutionListener()).build();
	}

	@Bean
	public StepExecutionListener getStepExecutionListener() {
		return new ExcelStepListener();
	}
}
