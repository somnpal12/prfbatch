package com.isit.prf.batch;

import java.util.Optional;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;

import com.isit.prf.batch.processor.ExcelSheet3ItemProcessor;
import com.isit.prf.batch.processor.ExcelSheet4ItemProcessor;
import com.isit.prf.batch.processor.ExcelSheet5ItemProcessor;
import com.isit.prf.batch.processor.ExcelSheet6ItemProcessor;
import com.isit.prf.batch.processor.ExcelSheet7ItemProcessor;
import com.isit.prf.batch.processor.ExcelSheet8ItemProcessor;
import com.isit.prf.data.ClientMasterRepository;
import com.isit.prf.entity.ClientMaster;
import com.isit.prf.util.ConstantUtil;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ExcelItemProcessor implements ItemProcessor<XSSFWorkbook, ClientMaster>{

	ClientMaster clientMaster = null;
		
	@Autowired
	ClientMasterRepository clientMasterRepository;
	
	@Autowired
	ExcelSheet3ItemProcessor sheet3Processor;
	
	@Autowired
	ExcelSheet4ItemProcessor sheet4Processor;
	
	@Autowired
	ExcelSheet5ItemProcessor sheet5Processor;
	
	@Autowired
	ExcelSheet6ItemProcessor sheet6Processor;
	
	@Autowired
	ExcelSheet7ItemProcessor sheet7Processor;
	
	@Autowired
	ExcelSheet8ItemProcessor sheet8Processor;
	
	
	StepExecution stepExecution;
	
	@BeforeStep
	public void beforeStep(StepExecution pStepExecution) {
		this.stepExecution = pStepExecution;
	}
	
	
	@Override
	public ClientMaster process(XSSFWorkbook workbook) throws Exception {
		
		Long id = (Long)stepExecution.getExecutionContext().get("CLIENT_MASTER");
		log.info("processing sheet for client master {}",id);
		Optional<ClientMaster> o  = clientMasterRepository.findById(id);
		if(o.isPresent()) {
			log.debug("data available in {}",id);
			clientMaster = o.get();
			
		
			
			for (Sheet sheet : workbook) {
						
				if(ConstantUtil.SHEET3.getValue().equals(sheet.getSheetName())) {
					sheet3Processor.process((XSSFSheet)sheet,clientMaster);
				}else if(ConstantUtil.SHEET4.getValue().equals(sheet.getSheetName())) {
					sheet4Processor.process((XSSFSheet)sheet,clientMaster);
				}else if(ConstantUtil.SHEET5.getValue().equals(sheet.getSheetName())) {
					sheet5Processor.process((XSSFSheet)sheet,clientMaster);
				}else if(ConstantUtil.SHEET6.getValue().equals(sheet.getSheetName())) {
					sheet6Processor.process((XSSFSheet)sheet,clientMaster);
				}else if(ConstantUtil.SHEET7.getValue().equals(sheet.getSheetName())) {
					sheet7Processor.process((XSSFSheet)sheet,clientMaster);
				}else if(ConstantUtil.SHEET8.getValue().equals(sheet.getSheetName())) {
					sheet8Processor.process((XSSFSheet)sheet,clientMaster);
				}
			}
			
			clientMaster.getPrfUserActivity().setStatus(ConstantUtil.DRAFTSTATUS.getValue());
		}
		
		log.info("processed sheet for client master {}",id);
		return clientMaster;
	}

	
	
}
