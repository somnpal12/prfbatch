package com.isit.prf.batch.processor;

import static com.isit.prf.util.ExcelUtils.getCellValue;
import static com.isit.prf.util.ExcelUtils.getCellValueAsString;
import static com.isit.prf.util.ExcelUtils.getCellValueByReference;
import static com.isit.prf.util.ExcelUtils.isHeaderRow;
import static com.isit.prf.util.ExcelUtils.isValidRow;

import java.util.HashSet;
import java.util.Set;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.springframework.stereotype.Component;

import com.isit.prf.entity.ClientInfoNamedInsured;
import com.isit.prf.entity.ClientInfoSubNamedInsured;
import com.isit.prf.entity.ClientMaster;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class ExcelSheet3ItemProcessor implements BaseItemProcessor{

	


	@Override
	public void process(XSSFSheet sheet,ClientMaster cMaster)  {
		
		log.info("processing sheet 3");
		
		cMaster.setClientInfoNamedInsured(mappedNamedInsured(sheet,cMaster));
		
		Set<ClientInfoSubNamedInsured> subInsuredCol = mappedSubNamedInsured(sheet,cMaster);
		if(! subInsuredCol.isEmpty()) {
			cMaster.setClientInfoSubNamedInsuredCol(subInsuredCol);
		}
		
		log.info("processed sheet 3");
		
	}
	
	
	private ClientInfoNamedInsured mappedNamedInsured(XSSFSheet sheet,ClientMaster cMaster) {
			
		
		return ClientInfoNamedInsured.builder()
				.firstNamedInsured(getCellValueByReference(sheet, "K9"))
				.mailingAddress(getCellValueByReference(sheet, "L9"))
				.businessType(getCellValueByReference(sheet, "M9"))
				.natureOfBusiness(getCellValueByReference(sheet, "N9"))
				.sic(getCellValueByReference(sheet, "O9"))
				.naics(getCellValueByReference(sheet, "P9"))
				.percentageOwn(getCellValueByReference(sheet, "Q9"))
				.noOfEmployee(getCellValueByReference(sheet, "R9"))
				.fein(getCellValueByReference(sheet, "S9"))
				.audit(getCellValueByReference(sheet, "T9"))
				.paymentPlan(getCellValueByReference(sheet, "U9"))
				.attribute1(getCellValueByReference(sheet, "V9"))
				.description(getCellValueByReference(sheet, "W9"))
				.tabId(sheet.getWorkbook().getSheetIndex(sheet.getSheetName()))
				.tabName(sheet.getSheetName())
				.clientMaster(cMaster)
				.build();
	}
	
	private Set<ClientInfoSubNamedInsured> mappedSubNamedInsured(XSSFSheet sheet , ClientMaster cMaster){
		Set<ClientInfoSubNamedInsured> subNamedInsuredCol = new HashSet<>();
		int startIndex = 10;
		int endIndex = 2000;
		ClientInfoSubNamedInsured subNamedInsured = null;	
		XSSFRow row = null;
		int category = 0;
		
		for(int i=startIndex ; i <endIndex ; i++) {
			row = sheet.getRow(i);
					
			if(isHeaderRow(row)) {
				category++;	
				continue;
			}
			
			
			if(isValidRow(row,"K"))	{
				subNamedInsured = ClientInfoSubNamedInsured.builder()
						.subName(getCellValue(row,"K"))
						.mailingAddress(getCellValue(row,"K"))
						.businessType(getCellValue(row,"M"))
						.natureOfBusiness(getCellValue(row,"N"))
						.sic(getCellValueAsString(row,"O"))
						.naics(getCellValueAsString(row,"P"))
						.percentageOwn(getCellValue(row,"Q"))
						.noOfEmployee(getCellValueAsString(row,"R"))
						.fein(getCellValueAsString(row,"S"))
						.managedByFirstNamedInsured(getCellValue(row,"T"))
						.hasPayroll(getCellValue(row,"U"))
						.entityStatusOnPolicy(getCellValue(row,"V"))
						.relationShipToNamedInsured(getCellValue(row,"W"))
						.tabId(sheet.getWorkbook().getSheetIndex(sheet.getSheetName()))
						.tabName(sheet.getSheetName())
						.attribute1(""+category)
						.clientMaster(cMaster)
						.build();
				
				subNamedInsuredCol.add(subNamedInsured);
				
			}	
		}
		
		
		return subNamedInsuredCol;
		
	}

	
	

}
