package com.isit.prf.batch.processor;

import org.apache.poi.xssf.usermodel.XSSFSheet;

import com.isit.prf.entity.ClientMaster;

public interface BaseItemProcessor {
	
	public void process(XSSFSheet sheet,ClientMaster cMaster) ;
	

}
