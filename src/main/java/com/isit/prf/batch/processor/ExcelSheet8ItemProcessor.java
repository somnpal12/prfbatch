package com.isit.prf.batch.processor;

import static com.isit.prf.util.ExcelUtils.getCellValueAsString;
import static com.isit.prf.util.ExcelUtils.isDataRow;

import java.util.Set;
import java.util.TreeSet;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.springframework.stereotype.Component;

import com.isit.prf.entity.BusinessIncomeWorksheet;
import com.isit.prf.entity.ClientMaster;
import com.isit.prf.util.ConstantUtil;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class ExcelSheet8ItemProcessor implements BaseItemProcessor {

	@Override
	public void process(XSSFSheet sheet, ClientMaster cMaster) {
		log.info("processing sheet 8");
		cMaster.setBusinessIncomeWorksheet(mappedBusinessIncomeWorksheet(sheet,cMaster));
		log.info("processed sheet 8");
	}

	private Set<BusinessIncomeWorksheet> mappedBusinessIncomeWorksheet(XSSFSheet sheet, ClientMaster cMaster) {
		Set<BusinessIncomeWorksheet> businessIncomeWorksheetList = new TreeSet<>((o1, o2)-> o1.getColumnId() < o2.getColumnId() ? o1.getColumnId() : o2.getColumnId());
		int startIndex = 7;
		int endIndex = 30;
		XSSFRow row = null;
		
	
		for(int i= startIndex ; i <endIndex ; i++) {
			row = sheet.getRow(i);
			
			if(isDataRow(row,"A")) {
				businessIncomeWorksheetList.add(getDataRow(sheet,row,cMaster));
			}
		}
		
		for(int j=34; j < 43 ; j++ ) {
			row = sheet.getRow(j);
			businessIncomeWorksheetList.add(getDataRow(sheet,row,cMaster));
		}
		
		
		return businessIncomeWorksheetList;
	}
	
	private BusinessIncomeWorksheet getDataRow(XSSFSheet sheet,XSSFRow row, ClientMaster cMaster) {
		return  BusinessIncomeWorksheet.builder()
				.columnId(row.getRowNum())
				.column1(getCellValueAsString(row,"L"))
				.column2(getCellValueAsString(row,"M"))
				.tabId(sheet.getWorkbook().getSheetIndex(sheet.getSheetName()))
				.tabName(sheet.getSheetName())
				.category(ConstantUtil.DEFAULT_CATEGORY.getInt())
			    .clientMaster(cMaster)
				.build(); 
		
	}
}
