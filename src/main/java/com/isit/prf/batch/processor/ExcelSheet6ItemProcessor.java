package com.isit.prf.batch.processor;

import static com.isit.prf.util.ExcelUtils.getCellValue;
import static com.isit.prf.util.ExcelUtils.getCellValueAsString;
import static com.isit.prf.util.ExcelUtils.isHeaderRow;
import static com.isit.prf.util.ExcelUtils.isValidRow;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.springframework.stereotype.Component;

import com.isit.prf.entity.ClientMaster;
import com.isit.prf.entity.PropertyInfo;
import com.isit.prf.util.ConstantUtil;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class ExcelSheet6ItemProcessor implements BaseItemProcessor{

	@Override
	public void process(XSSFSheet sheet,ClientMaster cMaster){
		log.info("processing sheet 6");
		
		cMaster.setPropertyInfo(mappedClientContact(sheet,cMaster));
		log.info("processed sheet 6");
	}
		
	private Set<PropertyInfo> mappedClientContact(XSSFSheet sheet ,ClientMaster cMaster) {
		Set<PropertyInfo> propertyInfoCol = new HashSet();
		int startIndex = 12;
		int endIndex = 2000;
		PropertyInfo propertyInfo = null;
		XSSFRow row = null;
		
		Long category = 0L;
		
		for(int i= startIndex ; i <endIndex ; i++) {
			row = sheet.getRow(i);
				
			if(isHeaderRow(row)) {
				category++;	
				continue;
			}
			
			if(isValidRow(row,"K")  ){
				
				if (StringUtils.contains(getCellValueAsString(row, "K"), ConstantUtil.FOOTER.getValue())) {
					break;
				}
					
				propertyInfo = PropertyInfo.builder()
						.locationId(getCellValueAsString(row,"K"))
						.buildId(getCellValueAsString(row,"L"))
						.building(getCellValue(row,"M"))
						.address(getCellValue(row,"N"))
						.city(getCellValue(row,"O"))
						.state(getCellValue(row,"P"))
						.zip(getCellValueAsString(row,"Q"))
						.constructionType(getCellValue(row,"R"))
						.totalArea(getCellValueAsString(row,"S"))
						.noOfStories(getCellValueAsString(row,"T"))
						.alarmType(getCellValue(row,"U"))
						.buildYear(getCellValueAsString(row,"V"))
						.asSprinker(getCellValue(row,"W"))
						.totalInsuredValue(getCellValueAsString(row,"X"))
						.description(getCellValue(row,"Y"))
						.dateOfChange(getCellValueAsString(row,"Z"))
						.financialInterest(getCellValue(row,"AA"))
						.tabId(sheet.getWorkbook().getSheetIndex(sheet.getSheetName()))
						.tabName(sheet.getSheetName())
						.category(category)
						.clientMaster(cMaster)
						.build();
				
				
				propertyInfoCol.add(propertyInfo);
				
			}
			
		}
		return propertyInfoCol;
	}
}
