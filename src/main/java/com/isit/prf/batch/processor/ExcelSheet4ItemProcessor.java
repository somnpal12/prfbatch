package com.isit.prf.batch.processor;

import static com.isit.prf.util.ExcelUtils.getCellValueAndCheckX;
import static com.isit.prf.util.ExcelUtils.getCellValueAsString;
import static com.isit.prf.util.ExcelUtils.isHeaderRow;
import static com.isit.prf.util.ExcelUtils.isValidRow;

import java.util.HashSet;
import java.util.Set;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.springframework.stereotype.Component;

import com.isit.prf.entity.ClientInfoCoverageList;
import com.isit.prf.entity.ClientMaster;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class ExcelSheet4ItemProcessor implements BaseItemProcessor{

		

	
	public void process(XSSFSheet sheet,ClientMaster cMaster)  {
		log.info("processing sheet 4");
		cMaster.setClientInfoCoverageList(mappedCoverageList(sheet,	cMaster));
		log.info("processed sheet 4");
	}
	
	
	private Set<ClientInfoCoverageList> mappedCoverageList(XSSFSheet sheet ,ClientMaster cMaster) {
		Set<ClientInfoCoverageList> coveragesCol = new HashSet<>();
		int startIndex = 9;
		int endIndex = 2000;
		ClientInfoCoverageList coverage = null;
		XSSFRow row = null;
		
		
		Long category = 0L;
		
		for(int i= startIndex ; i <endIndex ; i++) {
			row = sheet.getRow(i);
				
			if(isHeaderRow(row)) {
				category++;	
				continue;
			}
			
			if(isValidRow(row,"K")){
				
				coverage = ClientInfoCoverageList.builder()
							.namedInsured(getCellValueAsString(row, "K"))
							.property(getCellValueAndCheckX(row, "L"))
							.generalLiability(getCellValueAndCheckX(row, "M"))
							.inlandMarine(getCellValueAndCheckX(row, "N"))
							.auto(getCellValueAndCheckX(row, "O"))
							.workComp(getCellValueAndCheckX(row, "P"))
							.umbrella(getCellValueAndCheckX(row, "Q"))
							.crime(getCellValueAndCheckX(row, "R"))
							.pollution(getCellValueAndCheckX(row, "S"))
							.professional(getCellValueAndCheckX(row, "T"))
							.epli(getCellValueAndCheckX(row, "U"))
							.dAndO(getCellValueAndCheckX(row, "V"))
							.tabId(sheet.getWorkbook().getSheetIndex(sheet.getSheetName()))
							.tabName(sheet.getSheetName())
							.category(category)
							.clientMaster(cMaster)
							.build();
						
						coveragesCol.add(coverage);
			}
			
			
			
			
		}
		return coveragesCol;
	}

	

	
	

}
