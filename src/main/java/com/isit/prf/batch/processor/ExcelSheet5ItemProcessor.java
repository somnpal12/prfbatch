package com.isit.prf.batch.processor;

import static com.isit.prf.util.ExcelUtils.getCellValue;
import static com.isit.prf.util.ExcelUtils.getCellValueForSpecialCell;
import static com.isit.prf.util.ExcelUtils.isHeaderRow;
import static com.isit.prf.util.ExcelUtils.isValidRow;

import java.util.HashSet;
import java.util.Set;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.springframework.stereotype.Component;

import com.isit.prf.entity.ClientContact;
import com.isit.prf.entity.ClientMaster;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class ExcelSheet5ItemProcessor  implements BaseItemProcessor{

	@Override
	public void process(XSSFSheet sheet,ClientMaster cMaster)  {
		log.info("processing sheet 5");
		
		cMaster.setClientContact(mappedClientContact(sheet,cMaster));
		log.info("processed sheet 5");
	}
	
	
	private Set<ClientContact> mappedClientContact(XSSFSheet sheet ,ClientMaster cMaster) {
		Set<ClientContact> contactsCol = new HashSet<>();
		int startIndex = 9;
		int endIndex = 2000;
		ClientContact contacts = null;
		XSSFRow row = null;
		
		
		Long category = 0L;
		
		for(int i= startIndex ; i <endIndex ; i++) {
			row = sheet.getRow(i);
			
			
			
			if(isHeaderRow(row)) {
				category++;	
				continue;
			}
			
			if(isValidRow(row,"K") && row.getPhysicalNumberOfCells() >= 9){
				
				contacts = ClientContact.builder()
							.clientContact(getCellValue(row, "K"))
							.phone(getCellValueForSpecialCell(row, "L"))
							.fax(getCellValueForSpecialCell(row, "M"))
							.email(getCellValue(row, "N"))
							.location(getCellValue(row, "O"))
							.contactPoint(getCellValue(row, "P"))
							.notes(getCellValue(row, "Q"))
							.tabId(sheet.getWorkbook().getSheetIndex(sheet.getSheetName()))
							.tabName(sheet.getSheetName())
							.category(category)
							.clientMaster(cMaster)
							.build();
						
				contactsCol.add(contacts);
			}
			
			
			
			
		}
		return contactsCol;
	}

	

	
	

}
