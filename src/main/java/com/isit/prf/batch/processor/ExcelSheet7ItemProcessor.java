package com.isit.prf.batch.processor;

import static com.isit.prf.util.ExcelUtils.getCellValue;
import static com.isit.prf.util.ExcelUtils.getCellValueAsString;
import static com.isit.prf.util.ExcelUtils.getCellValueByReference;
import static com.isit.prf.util.ExcelUtils.isHeaderRow;
import static com.isit.prf.util.ExcelUtils.isValidRow;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.springframework.stereotype.Component;

import com.isit.prf.entity.ClientMaster;
import com.isit.prf.entity.StatementOfValueHeader;
import com.isit.prf.entity.StatementOfValueList;
import com.isit.prf.util.ConstantUtil;

import lombok.extern.slf4j.Slf4j;
@Slf4j
@Component
public class ExcelSheet7ItemProcessor implements BaseItemProcessor{

	
	
	@Override
	public void process(XSSFSheet sheet,ClientMaster cMaster){
		log.info("processing sheet 7");
		
		cMaster.setStatementOfValueHeader(mappedStatementOfValueHeader(sheet,cMaster));
		cMaster.setStatementOfValueLists(mappedStatementOfValueList(sheet,cMaster));
		log.info("processed sheet 7");
	}
		
	private StatementOfValueHeader mappedStatementOfValueHeader(XSSFSheet sheet ,ClientMaster cMaster) {
		
		return StatementOfValueHeader.builder()
				.coInsurance(getCellValueByReference(sheet,"N5"))
				.policyDeductibles(getCellValueByReference(sheet,"N6"))
				.building(getCellValueByReference(sheet,"N7"))
				.personalProperty(getCellValueByReference(sheet,"N8"))
				.agreedValue(getCellValueByReference(sheet,"Q5"))
				.blankedCoverage(getCellValueByReference(sheet,"Q6"))
				.mechanicalBreakdown(getCellValueByReference(sheet,"Q7"))
				.mechanicalBreakdownLimit(getCellValueByReference(sheet,"Q8"))
				.buildingValuation(getCellValueByReference(sheet,"U5"))
				.personalPropertyValuaton(getCellValueByReference(sheet,"U6"))
				.earthquakeLimit(getCellValueByReference(sheet,"U7"))
				.floodLimit(getCellValueByReference(sheet,"U8"))
				.earthquakeDeductible(getCellValueByReference(sheet,"W7"))
				.floodDeductible(getCellValueByReference(sheet,"W8"))
				.tabId(sheet.getWorkbook().getSheetIndex(sheet.getSheetName()))
				.tabName(sheet.getSheetName())
				.category(ConstantUtil.DEFAULT_CATEGORY.getInt())
			    .clientMaster(cMaster)
				.build();
	
		
	}
	
	
	
	private Set<StatementOfValueList> mappedStatementOfValueList(XSSFSheet sheet ,ClientMaster cMaster) {
		Set<StatementOfValueList> statementOfValueLists = new HashSet();
		int startIndex = 12;
		int endIndex = 2000;
		StatementOfValueList statementOfValueList = null;
		XSSFRow row = null;
		
		Long category = 0L;
		
		for(int i= startIndex ; i <endIndex ; i++) {
			row = sheet.getRow(i);
				
			if(isHeaderRow(row)) {
				category++;	
				continue;
			}
			
			if(isValidRow(row,"K")  ){
				
				if (StringUtils.contains(getCellValueAsString(row, "K"), ConstantUtil.FOOTER.getValue())) {
					break;
				}
					
				statementOfValueList = StatementOfValueList.builder()
						.location(getCellValueAsString(row,"K"))
						.building(getCellValueAsString(row,"L"))
						.buildingOccupancy(getCellValue(row,"M"))
						.address(getCellValue(row,"N"))
						.city(getCellValue(row,"O"))
						.state(getCellValue(row,"P"))
						.column1(getCellValueAsString(row,"Q"))
						.column2(getCellValueAsString(row,"R"))
						.column3(getCellValueAsString(row,"S"))
						.column4(getCellValueAsString(row,"T"))
						.column5(getCellValueAsString(row,"U"))
						.column6(getCellValueAsString(row,"V"))
						.column7(getCellValueAsString(row,"W"))
						.column8(getCellValueAsString(row,"X"))
						.column9(getCellValueAsString(row,"Y"))
						.totalLocation(getCellValueAsString(row,"Z"))
						.tabId(sheet.getWorkbook().getSheetIndex(sheet.getSheetName()))
						.tabName(sheet.getSheetName())
						.category(category)
						.clientMaster(cMaster)
						.build();
				
				
				statementOfValueLists.add(statementOfValueList);
				
			}
			
		}
		return statementOfValueLists;
	}
}
