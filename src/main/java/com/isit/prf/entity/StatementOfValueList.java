package com.isit.prf.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;


@Data
@SuperBuilder
@NoArgsConstructor
@Entity
@Table(name="T_STATEMENT_OF_VALUE_LIST")
public class StatementOfValueList extends MinBaseEntity implements Serializable{

	private static final long serialVersionUID = 6279027558103973226L;
	
	@Column(name="LOCATION")
	private String location;
	
	@Column(name="BUILDING_ID")
	private String building;
	
	@Column(name="BUILDING_NAME")
	private String buildingOccupancy;
	
	@Column(name="ADDRESS")
	private String address;
	
	@Column(name="CITY")
	private String city;
	
	@Column(name="STATE")
	private String state;
	
	@Column(name="COLUMN1")	
	private String column1;
	
	@Column(name="COLUMN2")
	private String column2;
	
	@Column(name="COLUMN3")
	private String column3;
	
	@Column(name="COLUMN4")
	private String column4;
	
	@Column(name="COLUMN5")
	private String column5;
	
	@Column(name="COLUMN6")
	private String column6;
	
	@Column(name="COLUMN7")
	private String column7;
	
	@Column(name="COLUMN8")
	private String column8;
	
	@Column(name="COLUMN9")
	private String column9;
	
	@Column(name="TOTAL_LOCATION")	
	private String totalLocation;
	
	@ManyToOne
	@JoinColumn(name="CLIENT_ID")
	public ClientMaster clientMaster;
	

}
