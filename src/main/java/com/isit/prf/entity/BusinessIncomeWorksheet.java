package com.isit.prf.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;


@Data
@SuperBuilder
@NoArgsConstructor
@Entity
@Table(name="T_BUSINESS_INCOME_WORKSHEET")
public class BusinessIncomeWorksheet extends MinBaseEntity implements Serializable{

	
	@Column(name="COLUMN_ID")
	private int columnId;
	
	@Column(name="COLUMN1")
	private String column1;
	
	@Column(name="COLUMN2")
	private String column2;
	
	@ManyToOne
	@JoinColumn(name="CLIENT_ID")
	public ClientMaster clientMaster;

}
