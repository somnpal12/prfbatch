package com.isit.prf.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="PRF_USER_ACTIVITY")
public class PrfUserActivity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3833905763344502235L;

	@Id
	@Column(name="PRF_ACTIVITY_ID")	
	private Long prfActivityId;
	
	@OneToOne
	@JoinColumn(name="CLIENT_ID")
	public ClientMaster clientMaster;
	
	@Column(name="YEAR_CODE")	
	public String yearCode;
	
	@Column(name="STATUS")	
	public String status;
	
	@Column(name="ASSIGNED_USER_TYPE")	
	public String userType;
	
	@Column(name="ASSIGNED_BY_CSR")	
	public String asgndByCSR;
	
	@Column(name="ASSIGNED_TO_CSR")	
	public String asgndToCSR;
	
	@Column(name="ASSIGNED_BY_CLIENT")	
	public String asgndByClient;
	
	@Column(name="ASSIGNED_TO_CLIENT")	
	public String asgndToClient;
	
	@Column(name="ACTIVITY_MESSAGE")	
	public String activityMsg;
	
	@Column(name="ADDED_BY")	
	public String addedBy;
	
	@Column(name="ADDED_DATE")	
	public Date addedDate;
	
	@Column(name="EDITED_BY")	
	public String editedBy;
	
	@Column(name="EDITED_DATE")	
	public Date editedDate;
	
	@Column(name="DISABLED_BY")	
	public String disabledBy;
	
	@Column(name="DISABLED_DATE")	
	public Date disabledDate;
	
}
