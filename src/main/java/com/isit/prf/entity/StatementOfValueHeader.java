package com.isit.prf.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;


@Data
@SuperBuilder
@NoArgsConstructor
@Entity
@Table(name="T_STATEMENT_OF_VALUE_HEADER")
public class StatementOfValueHeader extends MinBaseEntity implements Serializable{

	private static final long serialVersionUID = -9182012736631233429L;
	
	@Column(name="COINSURANCE")
	private String coInsurance;
	
	@Column(name="POLICY_DEDUCTIBLE")
	private String policyDeductibles;
	
	@Column(name="BUILDING")
	private String building;
	
	@Column(name="PERSONAL_PROPERTY")
	private String personalProperty;
	
	@Column(name="AGREED_VALUE")
	private String agreedValue;
	
	@Column(name="BLANKET_COVERAGE")
	private String blankedCoverage;
	
	@Column(name="MECHANICAL_BREAKDOWN")
	private String mechanicalBreakdown;
	
	@Column(name="MECHANICAL_BREAKDOWN_LIMIT")
	private String mechanicalBreakdownLimit;
	
	@Column(name="BUILDING_VALUATION")	
	private String buildingValuation;
	
	@Column(name="PERSONAL_PROPERTY_VALUATION")
	private String personalPropertyValuaton;
	
	@Column(name="EARTHQUAKE_LIMIT")
	private String earthquakeLimit;
	
	@Column(name="FLOOD_LIMIT")
	private String floodLimit;
	
	@Column(name="EARTHQUAKE_DEDUCTIBLE")	
	private String earthquakeDeductible;
	
	@Column(name="FLOOD_DEDUCTIBLE")
	private String floodDeductible;

	@OneToOne
	@JoinColumn(name="CLIENT_ID")
	public ClientMaster clientMaster;
}
