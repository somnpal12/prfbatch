package com.isit.prf.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@ToString
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="T_CLIENT_INFO_NAMED_INSURED")
public class ClientInfoNamedInsured extends BaseEntity implements Serializable{


	private static final long serialVersionUID = -6691385031628324433L;

	@Column(name="FIRST_NAMED_INSURED")
	private String firstNamedInsured;
	
	@Column(name="MAILING_ADDRESS")
	private String mailingAddress;
	
	@Column(name="BUSINESS_TYPE")
	private String businessType;
	
	@Column(name="NATURE_OF_BUSINESS")
	private String natureOfBusiness;
	
	@Column(name="SIC")
	private String sic;
	
	@Column(name="NAICS")
	private String naics;
	
	@Column(name="PERCENTAGE_OWNED")
	private String percentageOwn;
	
	@Column(name="NO_OF_EMPLOYEE")
	private String noOfEmployee;
	
	@Column(name="FEIN")
	private String fein;
	
	@Column(name="AUDIT")
	private String audit;
	
	@Column(name="PAYMENT_PLAN")
	private String paymentPlan;
	
	@Column(name="DESCRIPTION")
	private String description;
	
		
	@OneToOne
	@JoinColumn(name="HEADER_ID")
	public ClientMaster clientMaster;
	
}
