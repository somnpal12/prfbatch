package com.isit.prf.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.isit.prf.util.ConstantUtil;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@MappedSuperclass

@EntityListeners(AuditingEntityListener.class)
public class MinBaseEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name="STATUS")
	private String status;

	@CreatedBy
	@Column(name="CREATED_BY")
	private String createdBy;

	@CreatedDate
	@Column(name="CREATED_ON")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdOn ;
	
	@Column(name = "TAB_ID")
	private int tabId;

	@Column(name = "TAB_NAME")
	private String tabName;
	
	@Version
	@Column(name="VERSION")
	private Long version;
	
	@Column(name="CATEGORY")
	private Long category;
	
	@Column(name="COLORED")
	private Long colored;
	
	@PrePersist 
    public void prePersist() {
      this.colored = ConstantUtil.DEFAULT_COLOR.getInt();
      this.status = ConstantUtil.STATUS.getValue();
      
      if(this.category == null) {
    	  this.category = ConstantUtil.DEFAULT_CATEGORY.getInt();
      }
    
    		  
    }
}
