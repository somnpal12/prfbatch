package com.isit.prf.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@ToString
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="T_CLIENT_INFO_SUB_NAMED_INSURED")
public class ClientInfoSubNamedInsured extends BaseEntity implements Serializable{

		
	/**
	 * 
	 */
	private static final long serialVersionUID = 5926283116367751303L;

	@Column(name="SUB_NAME")
	private String subName;
	
	@Column(name="MAILING_ADDRESS")
	private String mailingAddress;
	
	@Column(name="BUSINESS_TYPE")
	private String businessType;
	
	
	@Column(name="NATURE_OF_BUSINESS")
	private String natureOfBusiness;
	
	@Column(name="SIC")
	private String sic;
	
	@Column(name="NAICS")
	private String naics;
	
	@Column(name="PERCENTAGE_OWNED")
	private String percentageOwn;
	
	@Column(name="NO_OF_EMPLOYEE")
	private String noOfEmployee;
	
	@Column(name="FEIN")
	private String fein;
	
	
	@Column(name="IS_MANAGED_BY_FIRST_NAMED_INSURED")
	private String managedByFirstNamedInsured;
	
	@Column(name="HAS_PAYROLL")
	private String hasPayroll;
	
	@Column(name="ENTITY_STATUS_ON_POLICY")
	private String entityStatusOnPolicy;
	
	@Column(name="RELATIONSHIP_TO_NAMED_INSURED")
	private String relationShipToNamedInsured;
	
	
	
	@ManyToOne
	@JoinColumn(name="HEADER_ID")
	public ClientMaster clientMaster;
}
