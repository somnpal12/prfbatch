package com.isit.prf.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@Entity
@Table(name="T_CLIENT_CONTACT_LIST")
public class ClientContact extends BaseEntity implements Serializable{

	
	private static final long serialVersionUID = 5882083355503093126L;
	
	@Column(name = "CLIENT_CONTACT")
	private String clientContact;
	
	@Column(name = "PHONE")
	private String phone;
	
	@Column(name = "FAX")
	private String fax;
	
	@Column(name = "EMAIL")
	private String email;
	
	@Column(name = "LOCATION")
	private String location;
	
	@Column(name = "CONTACT_POINT")
	private String contactPoint;
	
	@Column(name = "NOTES")
	private String notes;
	
	@ManyToOne
	@JoinColumn(name="CLIENT_ID")
	public ClientMaster clientMaster;
	
}
