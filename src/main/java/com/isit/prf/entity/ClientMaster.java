package com.isit.prf.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="CLIENT_MASTER")
@NamedQuery(name = "ClientMaster.findByClientName", query = "SELECT p FROM ClientMaster p WHERE LOWER(p.clientName) = LOWER(?1)")
public class ClientMaster implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8525861755114404244L;

	@Id
	@Column(name="CLIENT_ID")	
	private Long clientId;
	
	@OneToOne(mappedBy="clientMaster" ,cascade= CascadeType.ALL, fetch=FetchType.LAZY)
	private PrfUserActivity prfUserActivity;
	
	@OneToOne(mappedBy="clientMaster" ,cascade= CascadeType.ALL)
	private ClientInfoNamedInsured clientInfoNamedInsured;
	
	
	@OneToMany(mappedBy="clientMaster" ,cascade= CascadeType.ALL)
	private Set<ClientInfoSubNamedInsured> clientInfoSubNamedInsuredCol;
	
	@OneToMany(mappedBy="clientMaster" ,cascade= CascadeType.ALL)
	private Set<ClientInfoCoverageList> clientInfoCoverageList;
	
	@OneToMany(mappedBy="clientMaster" ,cascade= CascadeType.ALL)
	private  Set<ClientContact> clientContact;
	
	@OneToMany(mappedBy="clientMaster" ,cascade= CascadeType.ALL)
	private  Set<PropertyInfo> propertyInfo;
	
	
	@OneToOne(mappedBy="clientMaster" ,cascade= CascadeType.ALL)
	private StatementOfValueHeader statementOfValueHeader;
	
	
	@OneToMany(mappedBy="clientMaster" ,cascade= CascadeType.ALL)
	private Set<StatementOfValueList> statementOfValueLists;
	
	
	@OneToMany(mappedBy="clientMaster" ,cascade= CascadeType.ALL)
	private Set<BusinessIncomeWorksheet> businessIncomeWorksheet;
	
	@Column(name="CLIENT_NAME")	
	private String clientName;
	
	@Column(name="YEAR_CODE")	
	private String yearCode;
	
	@Column(name="UPLOADEDFILE_NAME")	
	private String filename;
	
	@Column(name="UPLOADEDFILE_PATH")	
	private String filepath;
	
	@Column(name="BUSINESS_PHONE")	
	private String phone;
	
	@Column(name="ADDRESS")	
	private String address;
	
	@Column(name="BUSINESS_EMAIL")	
	private String businessEmail;
	
	@Column(name="CONTACT_PREFIX")	
	private String prefix;
	
	@Column(name="CONTACT_FIRSTNAME")	
	private String firstname;
	
	@Column(name="CONTACT_MIDDLENAME")	
	private String middlename;
	
	@Column(name="CONTACT_LASTNAME")	
	private String lastname;
	
	@Column(name="CONTACT_MOBILE")	
	private String contactMobile;
	
	@Column(name="CONTACT_EMAIL")	
	private String contactEmail;
	
	@Column(name="ADDED_BY")	
	private String addedBy;
	
	@Column(name="ADDED_DATE")	
	private Date addedDate;
	
	@Column(name="EDITED_BY")	
	private String editedBy;
	
	@Column(name="EDITED_DATE")	
	private Date editedDate;
	
	@Column(name="DISABLED_BY")	
	private String disabledBy;
	
	@Column(name="DISABLED_DATE")	
	private Date disabledDate;
	
	
	
}
