package com.isit.prf.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@ToString
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="T_CLIENT_INFO_COVERAGE_LIST")
public class ClientInfoCoverageList extends BaseEntity implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 34306857945285438L;

	@Column(name = "NAMED_INSURED")
	private String namedInsured;
	
	@Column(name = "PROPERTY")
	private String property;
	
	@Column(name = "GENERAL_LIABILITY")
	private String generalLiability;
	
	@Column(name = "INLAND_MARINE")
	private String inlandMarine;
	
	@Column(name = "AUTO")
	private String auto;
	
	@Column(name = "WORK_COMP")
	private String workComp;
	
	@Column(name = "UMBRELLA")
	private String umbrella;
	
	@Column(name = "CRIME")
	private String crime;
	
	@Column(name = "POLLUTION")
	private String pollution;
	
	@Column(name = "PROFESSIONAL")
	private String professional;
	
	@Column(name = "EPLI")
	private String epli;
	
	@Column(name = "D_AND_O")
	private String dAndO;
	
	
	@ManyToOne
	@JoinColumn(name="HEADER_ID")
	public ClientMaster clientMaster;
}
