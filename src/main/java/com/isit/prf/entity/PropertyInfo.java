package com.isit.prf.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@Entity
@Table(name="T_PROPERTY_INFO")
public class PropertyInfo extends MinBaseEntity implements Serializable{
	
	
	private static final long serialVersionUID = 8507016725436138858L;
	
	@Column(name="LOCATION")
	private String locationId;
	
	@Column(name="BUILDING_ID")
	private String buildId;
	
	@Column(name="BUILDING_NAME")
	private String building;
	
	@Column(name="ADDRESS")
	private String address;
	
	@Column(name="CITY")
	private String city;
	
	@Column(name="STATE")
	private String state;
	
	@Column(name="ZIP")
	private String zip;
	
	@Column(name="ISO_CONSTRUCTION_TYPE")
	private String constructionType;
	
	@Column(name="TOTAL_AREA")
	private String totalArea;
	
	@Column(name="NO_OF_STORIES")
	private String noOfStories;
	
	@Column(name="ALARM_TYPE")
	private String alarmType;
	
	@Column(name="BUILD_YEAR")
	private String buildYear;
	
	@Column(name="AS_SPRINKER")
	private String asSprinker;
	
	@Column(name="TOTAL_SUM_ASSURED")	
	private String totalInsuredValue;
	
	@Column(name="MID_YEAR_CHANGE_DESC")
	private String description;
	
	@Column(name="DATE_OF_CHANGE")
	private String dateOfChange;
	
	@Column(name="FINANCIAL_INTEREST")
	private String financialInterest;
	
	@ManyToOne
	@JoinColumn(name="CLIENT_ID")
	public ClientMaster clientMaster;
	
		
}
