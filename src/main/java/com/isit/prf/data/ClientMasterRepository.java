package com.isit.prf.data;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.isit.prf.entity.ClientMaster;


@Repository
public interface ClientMasterRepository extends JpaRepository<ClientMaster, Long>{
	 public List<ClientMaster> findByClientName(String name);
	 
	 @Query("SELECT c FROM ClientMaster c INNER JOIN c.prfUserActivity pr WHERE pr.status = :status")
	 public List<ClientMaster> findByStatus(@Param("status") String status);
}
