package com.isit.prf.data;

import java.util.List;

import com.isit.prf.entity.PrfUserActivity;


public interface PrfUserActivityRepository /*extends JpaRepository<PrfUserActivity, Long> */{

	List<PrfUserActivity> findByStatus();
}
