package com.isit.prf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PrfBatchApplication {

	public static void main(String[] args) {

		SpringApplication.run(PrfBatchApplication.class, args);

	}
}
