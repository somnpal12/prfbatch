package com.isit.prf.util;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.ss.util.NumberToTextConverter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;

public class ExcelUtils {
	
	public static boolean isHeaderRow(XSSFRow row, String columnIndex) {
		return row != null 
				&& row.getCell(CellReference.convertColStringToIndex(columnIndex))!=null 
				&& StringUtils.equals(row.getCell(CellReference.convertColStringToIndex(columnIndex)).getStringCellValue(),"#") ;
	}
	
	public static boolean isHeaderRow(XSSFRow row) {
		return isHeaderRow(row,"A");
	}
	
	
	public static boolean isDataRow(XSSFRow row,String rowNum) {
		return isHeaderRow(row,rowNum);
	}

	public static String getCellValueAsString(XSSFCell sCell) {
		
		String cellVal = null;
		switch(sCell.getCellType().name()) 
		{
			case "NUMERIC" :
				cellVal = NumberToTextConverter.toText(sCell.getNumericCellValue());
				break;
			case "FORMULA" :
				cellVal = sCell.getRawValue();
				break;
			default :
				cellVal = sCell.getStringCellValue();
				break;	
		}
		
		return cellVal;
			
	}
	
	public static String getCellValueAsString(XSSFRow row, String columnIndex) {
		return getCellValueAsString(row.getCell(CellReference.convertColStringToIndex(columnIndex)));
	}
	
	public static boolean isValidRow(XSSFRow row, String columnIndex) {
		return row !=null 
				&& row.getCell(CellReference.convertColStringToIndex(columnIndex))!=null 
				&& StringUtils.isNotBlank(getCellValueAsString(row,columnIndex)) ;
	}
	
	public static String getCellValue(XSSFRow row, String columnIndex) {
		return row.getCell(CellReference.convertColStringToIndex(columnIndex)).getStringCellValue();
	}
	
	public static String getCellValueForSpecialCell(XSSFRow row, String columnIndex) {
		DataFormatter formatter = new DataFormatter();
		return formatter.formatCellValue(row.getCell(CellReference.convertColStringToIndex(columnIndex)));
		
	}


	public static String getCellValueAndCheckX(XSSFRow row, String columnIndex) {
		
		if(StringUtils.equalsIgnoreCase("X", row.getCell(CellReference.convertColStringToIndex(columnIndex)).getStringCellValue())) {
			return "1";
		}else {
			return "0";
		}
	
	}
	
	/**
	 * 
	 * @param sheet
	 * @param cellRef
	 * @return Value of Cell ( e.g. A5, B7 etc...)
	 */
	public static String getCellValueByReference(XSSFSheet sheet,String cellRef) {
		String s = null;
		CellReference ref = new CellReference(cellRef);
		XSSFRow r = sheet.getRow(ref.getRow());
		
		 if (r != null && r.getCell(ref.getCol()) != null) {
			 s = getCellValueAsString(r.getCell(ref.getCol()));
		 }
		 
		 return s;
	}
	
}
