package com.isit.prf.util;

public enum ConstantUtil {
	
	DEFAULT_CATEGORY("1"),
	DEFAULT_COLOR("0"),
	SHEET3("Named Insureds"),
	SHEET4("Named Insured Coverage List"),
	SHEET5("Client Contact Sheet"),
	SHEET6("Property Info"),
	SHEET7("Statement of Values"),
	SHEET8("Business Income Worksheet"),
	STATUS("ACTIVE"),
	ERRSTATUS("ERROR"),
	CREATEDBY("BATCH"),
	DRAFTSTATUS("DRAFT"),
	SEARCHBY("Uploaded"),
	//VERSION("0"),
	//SHEET6FOOTER("Insert additional Rows above this Row"),
	FOOTER("Insert additional Rows above this Row");
	
	private String val;
	
	ConstantUtil(String value){
		this.val = value ;
	}
	
	
	
	public String getValue() {
		return this.val;
	}
	
	public Long getInt() {
		return Long.parseLong(this.val);
	}

}
